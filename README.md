# Technologie Backendowe

 Przykłady z wykładów są dostępne na osobnych gałęziach. Aby przejść do przykładów należy skorzystać z komendy `git checkout nazwa_galezi`.

### Linki do przykładów z wykładu

- Wykład 4 (konteneryzacja aplikacji typu RESTful) - [Link](https://gitlab.com/mmiotk/technologie-backendowe/-/tree/lecture_3) - Komenda `git checkout lecture_3`.
